const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const pass = "password";

const app = express();
const port = 8000;

app.get('/', (req, res) => {
    res.send('Welcome! This is assignment 2.')
});

app.get('/encode/:echo', (req, res) => {
    res.send('Encoded text: ' + Vigenere.Cipher(pass).crypt(req.params.echo));
});

app.get('/decode/:echo', (req, res) => {
    res.send('Decoded text: ' + Vigenere.Decipher(pass).crypt(req.params.echo));
});

app.listen(port, () => {
    console.log('Live on port: ' + port)
});